import java.util.ArrayList;


public class Education {
	private String institution;
	private String area;
	private String studyType;
	private String startDate;
	private String endDate;
	private String gpa;
	private ArrayList<String> courses;
}
