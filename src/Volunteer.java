import java.lang.reflect.Array;
import java.util.ArrayList;


public class Volunteer {
	private String organization;
	private String position;
	private String website;
	private String startDate;
	private String endDate;
	private String summary;
	private ArrayList<String> highlights;
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public ArrayList<String> getHighlights() {
		return highlights;
	}
	public void setHighlights(ArrayList<String> highlights) {
		this.highlights = highlights;
	}
	public String getOrganization() {
		return organization;
	}
	public void setOrganization(String organization) {
		this.organization = organization;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}

}
