import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

/**
 * Servlet implementation class FirstServlet
 */
public class resumeparse extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final String HTML_START="<html><body align='center'>";
	public static final String HTML_END="</body></html>";
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public resumeparse() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		Date date = new Date();
		BufferedReader br = null;
		FileReader fr = null;
		try {
			fr = new FileReader(System.getProperty( "catalina.base")+"/webapps/resumeparse/json-resume.json");
			br = new BufferedReader(fr);
			out.println(HTML_START);
			String sCurrentLine, finalJsonString="";
			while ((sCurrentLine = br.readLine()) != null) {
				finalJsonString = finalJsonString + sCurrentLine;
			}
			Gson gson = new Gson();
 			 Resume resume= gson.fromJson(finalJsonString, Resume.class);
 			StringBuffer html = new StringBuffer();
 			html.append(HTML_START);
 			html.append("<h1>BASICS</h1><br/><h4>Name: </h4>"+resume.getBasics().getName());
 			html.append("<h4>Label: </h4>"+resume.getBasics().getLabel());
 			html.append("<h4>Picture: </h4>"+resume.getBasics().getPicture());
 			html.append("<h4>Email: </h4>"+resume.getBasics().getEmail());
 			html.append("<h4>Phone: </h4>"+resume.getBasics().getPhone());
 			html.append("<h4>Website: </h4>"+resume.getBasics().getWebsite());
 			html.append("<h4>Summary: </h4>"+resume.getBasics().getSummary());
 			html.append("<h3>Location: </h3>");
 			html.append("<h4>Address: </h4>"+resume.getBasics().getLocation().getAddress());
 			html.append("<h4>Postal Code: </h4>"+resume.getBasics().getLocation().getPostalCode());
 			html.append("<h4>City: </h4>"+resume.getBasics().getLocation().getCity());
 			html.append("<h4>Country Code: </h4>"+resume.getBasics().getLocation().getCountryCode());
 			html.append("<h4>Region: </h4>"+resume.getBasics().getLocation().getRegion());
 			html.append("<h2>Profile: </h2>");
 			for(Profile profile : resume.getBasics().getProfiles()){
 				html.append("<h4>Network: </h4>"+profile.getNetwork());
 				html.append("<h4>User Name: </h4>"+profile.getUsername());
 				html.append("<h4>URL: </h4>"+profile.getUrl());
 			}
 			html.append(HTML_END);
 			out.println(html);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();
				if (fr != null)
					fr.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}

